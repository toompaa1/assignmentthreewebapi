﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Services
{
    public interface ICharacterService
    {
        public Task<IEnumerable<Character>> GetAllCharacters();
        public Task<Character> GetCharacterById(int characterId);
        public Task<Character> PostCharacter(Character character);
        public Task DeleteCharacter(int characterId);
        public Task UpdateCharacter(Character character);
        public bool CharacterExists(int characterId);
    }
}
