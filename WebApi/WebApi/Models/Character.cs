﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class Character
    {
        public int CharacterId { get; set; }
        [MaxLength(50)]
        public string FullName { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        [MaxLength(10)]
        public string Gender { get; set; }
        [MaxLength(500)]
        public string Picture { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
