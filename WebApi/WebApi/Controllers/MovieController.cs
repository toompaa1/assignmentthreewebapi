﻿using WebApi.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using WebApi.Services;
using WebApi.DTOs.Movie;


namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MovieController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;
        public MovieController(IMovieService movieService, IMapper mapper, ICharacterService characterService)
        {
            _movieService = movieService;
            _mapper = mapper;
            _characterService = characterService;
        }

        /// <summary>
        /// Get all movies in database
        /// </summary>
        /// <returns> A list with all movies </returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAllMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMovies());
        }

        /// <summary>
        /// Get a movie by enter their IDv
        /// </summary>
        /// <param name="movieId">The ID of the movie you want to show</param>
        /// <returns>The movie with the entred ID</returns>
        [HttpGet("{movieId}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovieById(int movieId)
        {
            Movie movie = await _movieService.GetMovieById(movieId);
            if (movie == null)
            {
                return NotFound();
            }
            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Get all characters in a movie
        /// </summary>
        /// <param name="movieId">ID of a movie</param>
        /// <returns></returns>
        [HttpGet("{movieId}/characters")]
        public async Task<ActionResult<MovieCharacterDTO>> GetMovieCharacters(int movieId)
        {
            Movie movie = await _movieService.GetMovieCharacters(movieId);
            if (movie == null)
            {
                return NotFound();
            }
            return _mapper.Map<MovieCharacterDTO>(movie);
        }

        /// <summary>
        /// Creating a new movie and add it to the database
        /// </summary>
        /// <param name="dtoMovie">The movie you want to create</param>
        /// <returns>New movie you created </returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> CreateMovie(MovieCreateDTO dtoMovie)
        {
            Movie modelMovie = _mapper.Map<Movie>(dtoMovie);
            modelMovie = await _movieService.PostMovie(modelMovie);
            return CreatedAtAction("GetMovieById", new { movieId = modelMovie.MovieId }, _mapper.Map<MovieReadDTO>(modelMovie));
        }

        /// <summary>
        /// Deleting a existing movie from database
        /// </summary>
        /// <param name="movieId"> The Id of the franchise you want to delete </param>
        /// <returns> OK Response if everything went well, NotFound if the movie doesent exist</returns>
        [HttpDelete("{movieId}")]
        public async Task<IActionResult> DeleteMovie(int movieId)
        {
            if (!_movieService.MovieExists(movieId))
            {
                return NotFound();
            }
            await _movieService.DeleteMovie(movieId);
            return Ok();
        }

        /// <summary>
        /// Updating a movie in database
        /// </summary>
        /// <param name="movieId">the Id of the movie you want to update</param>
        /// <param name="dtoMovie">The parameters you want to update to</param>
        /// <returns>OK messages if everything went well,"NotFound" if movieId doesent exist, "BadRequest" if the update fails</returns>
        [HttpPut("{movieId}")]
        public async Task<IActionResult> UpdateMovie(int movieId, MovieUpdateDTO dtoMovie)
        {
            if (movieId != dtoMovie.MovieId)
            {
                return BadRequest();
            }
            if (!_movieService.MovieExists(movieId))
            {
                return NotFound();
            }
            Movie modelMovie = _mapper.Map<Movie>(dtoMovie);
            await _movieService.UpdateMovie(modelMovie);
            return Ok();
        }

        /// <summary>
        /// Addning characters to a movie
        /// </summary>
        /// <param name="movieId">The movie you want update</param>
        /// <param name="characters">A list of characters you want to add to the movie</param>
        /// <returns>OK messages if everything went well,"NotFound" if movieId doesent exist, "BadRequest" if the update fails</returns>
        [HttpPut("{movieId}/add/characters")]
        public async Task<IActionResult> UpdateMovieCharacters(int movieId, List<int> characters)
        {
            if (!_movieService.MovieExists(movieId))
            {
                return NotFound();
            }
            try
            {
                await _movieService.UpdateMovieCharacter(movieId, characters);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest();
            }
            return Ok();
        }

        /// <summary>
        /// Delete characters from a movie
        /// </summary>
        /// <param name="movieId">The movie you want update</param>
        /// <param name="characters">A list of characters you want to remove from a movie</param>
        /// <returns>OK messages if everything went well,"NotFound" if movieId doesent exist, "BadRequest" if the update fails</returns>
        [HttpPut("{movieId}/delete/characters")]
        public async Task<IActionResult> DeleteMovieCharacter(int movieId, List<int> characters)
        {
            if (!_movieService.MovieExists(movieId))
            {
                return NotFound();
            }
            try
            {
                await _movieService.DeleteMovieCharacter(movieId, characters);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}
