﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Services
{
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetAllMovies();
        public Task<Movie> GetMovieById(int movieId);
        public Task<Movie> PostMovie(Movie movie);
        public Task DeleteMovie(int movieId);
        public Task UpdateMovie(Movie movie);
        public Task UpdateMovieCharacter(int movieId, List<int> characters);
        public Task DeleteMovieCharacter(int movieId, List<int> characters);
        public Task<Movie> GetMovieCharacters(int movieId);
        public bool MovieExists(int movieId);
    }
}
