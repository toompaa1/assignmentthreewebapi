﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApi.DTOs.Character
{
    public class CharacterUpdateDTO
    {
        public int CharacterId { get; set; }
        [MaxLength(100)]
        public string FullName { get; set; }
        [MaxLength(100)]
        public string Alias { get; set; }
        [MaxLength(10)]
        public string Gender { get; set; }
        [MaxLength(500, ErrorMessage = "Picture is not valid")]
        public string Picture { get; set; }
    }
}
