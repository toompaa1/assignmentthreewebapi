﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieDbContext _context;
        public FranchiseService(MovieDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Franchise>> GetAllFranchises()
        {
            return await _context.Franchises.Include(f => f.Movies).ToListAsync();
        }
        public async Task<Franchise> GetFranchiseById(int franchiseId)
        {
            return await _context.Franchises.FindAsync(franchiseId);
        }
        public async Task<Franchise> GetMoviesByFranchiseId(int franchiseId)
        {
            Franchise franchiseMovies = await _context.Franchises.Include(m => m.Movies).Where(f => f.FranchiseId == franchiseId).FirstAsync();
            return franchiseMovies;
        }
        public async Task<IEnumerable<Character>> GetCharactersInFranchiseMovie(int franchiseId)
        {
            return await _context.Movies.Where(m => m.FranchiseId == franchiseId).SelectMany(x => x.Characters).Distinct().ToListAsync();
        }
        public async Task<Franchise> PostFranchise(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }
        public async Task DeleteFranchise(int franchiseId)
        {
            var franchise = await _context.Franchises.FindAsync(franchiseId);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateFranchise(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public bool FranchiseExists(int franchiseId)
        {
            return _context.Franchises.Any(f => f.FranchiseId == franchiseId);
        }
        public async Task UpdateMovieFranchise(int franchiseId, List<int> movies)
        {
            Franchise franchiseToUpdateMovie = await _context.Franchises
                .Include(f => f.Movies)
                .Where(f => f.FranchiseId == franchiseId)
                .FirstAsync();
            foreach (int movieId in movies)
            {
                Movie movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                {
                    throw new KeyNotFoundException();
                }
                franchiseToUpdateMovie.Movies.Add(movie);
            }
            await _context.SaveChangesAsync();
        }
        public async Task DeleteMovieFranchise(int franchiseId, List<int> movies)
        {
            Franchise franchiseToUpdateMovie = await _context.Franchises
                .Include(f => f.Movies)
                .Where(f => f.FranchiseId == franchiseId)
                .FirstAsync();
            foreach (int movieId in movies)
            {
                Movie movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                {
                    throw new KeyNotFoundException();
                }
                franchiseToUpdateMovie.Movies.Remove(movie);
            }
            await _context.SaveChangesAsync();
        }
    }
}
