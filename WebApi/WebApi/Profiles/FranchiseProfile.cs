﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using WebApi.Models;
using WebApi.DTOs.Franchise;

namespace WebApi.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(f => f.MovieId).ToArray()));
            CreateMap<Franchise, FranchiseMovieDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(f => f.MovieId).ToArray()));
            CreateMap<Franchise, FranchiseCharacterDTO>()
                .ForMember(fdto => fdto.Characters, opt => opt
                .MapFrom(f => f.Movies.Select(f => f.MovieId).ToArray()));
            CreateMap<IEnumerable<Character>, FranchiseCharacterDTO>()
                .ForMember(fdto => fdto.Characters, opt => opt
                .MapFrom(c => c.Select(c => c.CharacterId).ToArray()));
            CreateMap<FranchiseCreateDTO, Franchise>();
            CreateMap<FranchiseUpdateDTO, Franchise>();
        }
    }
}
