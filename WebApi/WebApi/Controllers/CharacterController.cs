﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;
using Microsoft.EntityFrameworkCore;
using WebApi.Services;
using AutoMapper;
using WebApi.DTOs.Character;
using System.Net.Mime;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharacterController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;
        public CharacterController(ICharacterService characterService, IMapper mapper)
        {
            _characterService = characterService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all characters in database
        /// </summary>
        /// <returns> A list with characters </returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetAllCharacters());
        }

        /// <summary>
        /// Get a Character by enter their ID
        /// </summary>
        /// <param name="characterId"> Id of the character </param>
        /// <returns> The Character with the entred ID </returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{characterId}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacterById(int characterId)
        {
            Character character = await _characterService.GetCharacterById(characterId);
            if (character == null)
            {
                return NotFound();
            }
            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Creating a new character and add it to the database
        /// </summary>
        /// <param name="dtoCharacter"> The charcter you want to create </param>
        /// <returns> New character you created </returns>
        [HttpPost]
        public async Task<ActionResult<Character>> CreateCharacter(CharacterCreateDTO dtoCharacter)
        {
            Character modelCharacter = _mapper.Map<Character>(dtoCharacter);
            modelCharacter = await _characterService.PostCharacter(modelCharacter);
            return CreatedAtAction("GetCharacterById", new { characterId = modelCharacter.CharacterId }, _mapper.Map<CharacterReadDTO>(modelCharacter));
        }

        /// <summary>
        /// Deleting a existing character from database
        /// </summary>
        /// <param name="characterId">The Id of the character you want to delete </param>
        /// <returns> OK Response if everything went well, NotFound if the character doesent exist </returns>
        [HttpDelete("{characterId}")]
        public async Task<IActionResult> DeleteCharacter(int characterId)
        {
            if (!_characterService.CharacterExists(characterId))
            {
                return NotFound();
            }
            await _characterService.DeleteCharacter(characterId);
            return Ok();
        }

        /// <summary>
        /// Updating a character in database
        /// </summary>
        /// <param name="characterId"> the Id of the character you want to update </param>
        /// <param name="dtoCharacter"> The parameters you want to update to </param>
        /// <returns> OK Response if everything went well, NotFound if the character doesent exist </returns>
        [HttpPut("{characterId}")]
        public async Task<IActionResult> UpdateCharacter(int characterId, CharacterUpdateDTO dtoCharacter)
        {
            if (characterId != dtoCharacter.CharacterId)
            {
                return BadRequest();
            }
            if (!_characterService.CharacterExists(characterId))
            {
                return NotFound();
            }
            Character modelCharacter = _mapper.Map<Character>(dtoCharacter);
            await _characterService.UpdateCharacter(modelCharacter);
            return Ok();
        }
    }
}
