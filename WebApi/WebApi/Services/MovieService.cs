﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Services
{
    public class MovieService : IMovieService
    {
        private readonly MovieDbContext _context;
        public MovieService(MovieDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Movie>> GetAllMovies()
        {
            return await _context.Movies.Include(m => m.Characters).ToListAsync();
        }
        public async Task<Movie> GetMovieById(int movieId)
        {
            return await _context.Movies.FindAsync(movieId);
        }
        public async Task<Movie> GetMovieCharacters(int movieId)
        {
            Movie movieCharacters = await _context.Movies.
                Include(c => c.Characters).
                Where(m => m.MovieId == movieId).
                FirstAsync();

            return movieCharacters;
        }
        public async Task<Movie> PostMovie(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }
        public async Task DeleteMovie(int movieId)
        {
            var movie = await _context.Movies.FindAsync(movieId);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateMovie(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public bool MovieExists(int movieId)
        {
            return _context.Movies.Any(m => m.MovieId == movieId);
        }
        public async Task UpdateMovieCharacter(int movieId, List<int> characters)
        {
            Movie characterToUpdateMovies = await _context.Movies
                .Include(c => c.Characters)
                .Where(c => c.MovieId == movieId)
                .FirstAsync();
            foreach (int characterId in characters)
            {
                Character character = await _context.Characters.FindAsync(characterId);
                if (character == null)
                {
                    throw new KeyNotFoundException();
                }
                characterToUpdateMovies.Characters.Add(character);
            }
            await _context.SaveChangesAsync();
        }
        public async Task DeleteMovieCharacter(int movieId, List<int> characters)
        {
            Movie characterToDeleteMovies = await _context.Movies
                .Include(c => c.Characters)
                .Where(c => c.MovieId == movieId)
                .FirstAsync();
            foreach (int characterID in characters)
            {
                Character character = await _context.Characters.FindAsync(characterID);
                if (character == null)
                {
                    throw new KeyNotFoundException();
                }
                characterToDeleteMovies.Characters.Remove(character);
            }
            await _context.SaveChangesAsync();
        }
    }
}
