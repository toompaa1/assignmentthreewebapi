﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApi.DTOs.Movie
{
    public class MovieUpdateDTO
    {
        public int MovieId { get; set; }
        [MaxLength(200)]
        public string MovieTitle { get; set; }
        [MaxLength(100)]
        public string Genre { get; set; }
        [MaxLength(100)]
        public string ReleaseDate { get; set; }
        [MaxLength(30)]
        public string Director { get; set; }
        [MaxLength(500)]
        public string Picture { get; set; }
        [MaxLength(500)]
        public string Trailer { get; set; }
    }
}
