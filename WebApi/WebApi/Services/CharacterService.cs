﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;
using Microsoft.EntityFrameworkCore;


namespace WebApi.Services
{
    public class CharacterService : ICharacterService
    {
        private readonly MovieDbContext _context;
        public CharacterService(MovieDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Character>> GetAllCharacters()
        {
            return await _context.Characters.Include(c => c.Movies).ToListAsync();
        }
        public async Task<Character> GetCharacterById(int characterId)
        {
            return await _context.Characters.FindAsync(characterId);
        }
        public async Task<Character> PostCharacter(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }
        public async Task DeleteCharacter(int characterId)
        {
            var charcater = await _context.Characters.FindAsync(characterId);
            _context.Characters.Remove(charcater);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateCharacter(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public bool CharacterExists(int characterId)
        {
            return _context.Characters.Any(c => c.CharacterId == characterId);
        }
    }
}
