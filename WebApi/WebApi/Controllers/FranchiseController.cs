﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Services;
using AutoMapper;
using WebApi.DTOs.Franchise;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchiseController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;
        public FranchiseController(IFranchiseService franchiseService, IMapper mapper, IMovieService movieService)
        {
            _movieService = movieService;
            _franchiseService = franchiseService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all franchises in database
        /// </summary>
        /// <returns> A list with all franchises </returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetAllFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchises());
        }

        /// <summary>
        /// Get a franchise by its ID
        /// </summary>
        /// <param name="franchiseId">The ID of the movie you want to show</param>
        /// <returns>The franchise with the entred ID </returns>
        [HttpGet("{franchiseId}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseById(int franchiseId)
        {
            Franchise franchise = await _franchiseService.GetFranchiseById(franchiseId);
            if (franchise == null)
            {
                return NotFound();
            }
            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Get all movies in a franschise
        /// </summary>
        /// <param name="franchiseId"> Id of a franchise </param>
        /// <returns>List of movies </returns>
        [HttpGet("{franchiseId}/movies")]
        public async Task<ActionResult<FranchiseMovieDTO>> GetMoviesByFranchiseId(int franchiseId)
        {
            Franchise franchise = await _franchiseService.GetMoviesByFranchiseId(franchiseId);
            if (franchise == null)
            {
                return NotFound();
            }
            return _mapper.Map<FranchiseMovieDTO>(franchise);
        }

        /// <summary>
        ///  Get all characters in a franchise
        /// </summary>
        /// <param name="franchiseId"> Id of a franchise </param>
        /// <returns> a list och characters </returns>
        [HttpGet("{franchiseId}/characters")]
        public async Task<ActionResult<FranchiseCharacterDTO>> GetCharactesInFranchiseMovie(int franchiseId)
        {
            var characters = await _franchiseService.GetCharactersInFranchiseMovie(franchiseId);
            if (characters == null)
            {
                return NotFound();
            }
            return _mapper.Map<FranchiseCharacterDTO>(characters);
        }

        /// <summary>
        /// Creating a new franchise and add it to the database
        /// </summary>
        /// <param name="dtoFranchise">The franchise you want to create</param>
        /// <returns>New franchise you created</returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> CreateFranchise(FranchiseCreateDTO dtoFranchise)
        {
            Franchise modelFranchise = _mapper.Map<Franchise>(dtoFranchise);
            modelFranchise = await _franchiseService.PostFranchise(modelFranchise);
            return CreatedAtAction("GetFranchiseById", new { franchiseId = modelFranchise.FranchiseId }, _mapper.Map<FranchiseReadDTO>(modelFranchise));
        }

        /// <summary>
        /// Deleting a existing franchise from database
        /// </summary>
        /// <param name="franchiseId">The Id of the franchise you want to delete</param>
        /// <returns>OK Response if everything went well, NotFound if the franchise doesent exist</returns>
        [HttpDelete("{franchiseId}")]
        public async Task<IActionResult> DeleteFranchise(int franchiseId)
        {
            if (!_franchiseService.FranchiseExists(franchiseId))
            {
                return NotFound();
            }
            Franchise franchise = await _franchiseService.GetFranchiseById(franchiseId);

            foreach (Movie movie in await _movieService.GetAllMovies())
            {
                if (movie.FranchiseId == franchise.FranchiseId)
                {
                    movie.FranchiseId = null;
                }
            }
            await _franchiseService.DeleteFranchise(franchiseId);
            return Ok();
        }

        /// <summary>
        /// Updating a franchise in database
        /// </summary>
        /// <param name="franchiseId">the Id of the franchise you want to update </param>
        /// <param name="dtoFranchise">The parameters you want to update to </param>
        /// <returns>OK Response if everything went well, NotFound if the character doesent exist</returns>
        [HttpPut("{franchiseId}")]
        public async Task<IActionResult> UpdateFranchise(int franchiseId, FranchiseUpdateDTO dtoFranchise)
        {
            if (franchiseId != dtoFranchise.FranchiseId)
            {
                return BadRequest();
            }
            if (!_franchiseService.FranchiseExists(franchiseId))
            {
                return NotFound();
            }
            Franchise modelFranchise = _mapper.Map<Franchise>(dtoFranchise);
            await _franchiseService.UpdateFranchise(modelFranchise);
            return Ok();
        }

        /// <summary>
        /// Adding a movie to a franchise
        /// </summary>
        /// <param name="franchiseId">The franchise you want update </param>
        /// <param name="movies">The movie you want to add to the franchise </param>
        /// <returns> OK messages if everything went well,"NotFound" if franchiseId doesent exist, "BadRequest" if the update fails </returns>
        [HttpPut("{franchiseId}/add/movies")]
        public async Task<IActionResult> UpdateMovieFranchise(int franchiseId, List<int> movies)
        {
            if (!_franchiseService.FranchiseExists(franchiseId))
            {
                return NotFound();
            }
            try
            {
                await _franchiseService.UpdateMovieFranchise(franchiseId, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest();
            }
            return Ok();
        }

        /// <summary>
        /// Remove a movie from a franchise
        /// </summary>
        /// <param name="franchiseId"> The franchise you want update</param>
        /// <param name="movies">The movie you want to remove from the franchise</param>
        /// <returns>OK messages if everything went well,"NotFound" if franchiseId doesent exist, "BadRequest" if the update fails</returns>
        [HttpPut("{franchiseId}/delete/movies")]
        public async Task<IActionResult> DeleteMovieFranchise(int franchiseId, List<int> movies)
        {
            if (!_franchiseService.FranchiseExists(franchiseId))
            {
                return NotFound();
            }
            try
            {
                await _franchiseService.DeleteMovieFranchise(franchiseId, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}
