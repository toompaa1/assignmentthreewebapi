﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DTOs.Franchise
{
    public class FranchiseMovieDTO
    {
        public List<int> Movies { get; set; }
    }
}
