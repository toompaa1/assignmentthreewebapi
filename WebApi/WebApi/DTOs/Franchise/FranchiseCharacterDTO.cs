﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DTOs.Franchise
{
    public class FranchiseCharacterDTO
    {
        public List<int> Characters { get; set; }
    }
}
