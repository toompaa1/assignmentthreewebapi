﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DTOs.Movie
{
    public class MovieCharacterDTO
    {
        public List<int> Characters { get; set; }
    }
}
