﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Services
{
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> GetAllFranchises();
        public Task<Franchise> GetFranchiseById(int franchiseId);
        public Task<Franchise> PostFranchise(Franchise franchise);
        public Task DeleteFranchise(int franchiseId);
        public Task UpdateFranchise(Franchise franchise);
        public Task UpdateMovieFranchise(int franchiseId, List<int> movies);
        public Task DeleteMovieFranchise(int franchiseId, List<int> movies);
        public Task<Franchise> GetMoviesByFranchiseId(int franchiseId);
        public bool FranchiseExists(int franchiseId);
        public Task<IEnumerable<Character>> GetCharactersInFranchiseMovie(int franchiseId);
    }
}
