﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class Movie
    {
        public int MovieId { get; set; }
        [MaxLength(200)]
        public string MovieTitle { get; set; }
        [MaxLength(100)]
        public string Genre { get; set; }
        [MaxLength(100)]
        public string ReleaseDate { get; set; }
        [MaxLength(30)]
        public string Director { get; set; }
        [MaxLength(500)]
        public string Picture { get; set; }
        [MaxLength(500)]
        public string Trailer { get; set; }
        public int? FranchiseId { get; set; }
        public ICollection<Character> Characters { get; set; }
        public Franchise Franchises { get; set; }
    }
}
