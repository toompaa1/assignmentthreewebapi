﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Models
{
    public class MovieDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Character> Characters { get; set; }
        public MovieDbContext(DbContextOptions options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Character>().HasData(new Character
            {
                CharacterId = 1,
                FullName = "Harry Potter",
                Alias = "",
                Gender = "Male",
                Picture = "https://th.bing.com/th/id/R.4ca0b44a499fc6d7f53ff1c9e33fed34?rik=JiLEHDRjA3qCog&pid=ImgRaw",
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                CharacterId = 2,
                FullName = "Ron Wesley",
                Alias = "",
                Gender = "Male",
                Picture = "",
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                CharacterId = 3,
                FullName = "Hermione Granger",
                Alias = "",
                Gender = "Female",
                Picture = "https://static.wikia.nocookie.net/harrypotter/images/1/1e/Hermione.png/revision/latest/to[…]op/width/360/height/450?cb=20120731184935&path-prefix=sv",
            });
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                FranchiseId = 1,
                Name = "Warner Bros",
                Description = "Big company with alot good movies",
            });
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                FranchiseId = 2,
                Name = "Universal pictures",
                Description = "Old but gold",
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                MovieId = 1,
                MovieTitle = "Harry Potter philosofer stone",
                Genre = "Fantasy",
                ReleaseDate = "2011-10-14",
                Director = "J.K Rowling",
                Picture = "https://img.ecartelera.com/noticias/53400/53400-m.jpg",
                Trailer = "https://youtu.be/3EGojp4Hh6I",
                FranchiseId = 1,
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                MovieId = 2,
                MovieTitle = "Harry Potter and the deathly hallows",
                Genre = "Fantasy",
                ReleaseDate = "2015-09-27",
                Director = "J.K Rowling",
                Picture = "https://img.ecartelera.com/noticias/53400/53400-m.jpg",
                Trailer = "https://youtu.be/3EGojp4Hh6I",
                FranchiseId = 1,
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                MovieId = 3,
                MovieTitle = "Fast and furious",
                Genre = "Action",
                ReleaseDate = "2017-04-10",
                Director = "Gary Scott Thompson",
                Picture = "https://img.ecartelera.com/noticias/53400/53400-m.jpg",
                Trailer = "https://youtu.be/3EGojp4Hh6I",
                FranchiseId = 2,
            });
            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "CharactersMovie",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("MovieId", "CharacterId");
                        je.HasData(
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 1, MovieId = 2 },
                            new { CharacterId = 2, MovieId = 1 },
                            new { CharacterId = 3, MovieId = 1 },
                            new { CharacterId = 2, MovieId = 2 }
                        );
                    });
        }

    }
}
